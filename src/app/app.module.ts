import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OverlayModule, OverlayContainer, FullscreenOverlayContainer } from '@angular/cdk/overlay';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
	MatButtonModule,
	MatIconModule,
	MatInputModule,
	MatProgressSpinnerModule,
	MatCheckboxModule,
} from '@angular/material';


import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { ThankyouComponent } from './thankyou/thankyou.component';
import { ProgressOverlayComponent } from './progress-overlay/progress-overlay.component';

const appRoutes: Routes = [
	{ path: '', component: HomeComponent },
	{ path: 'register', component: RegisterComponent },
	{ path: 'thankyou', component: ThankyouComponent },
];

@NgModule({
	declarations: [
		AppComponent,
		HomeComponent,
		RegisterComponent,
		ThankyouComponent,
		ProgressOverlayComponent
	],
	imports: [
		BrowserModule,
		RouterModule.forRoot(appRoutes),
		FlexLayoutModule,
		MatButtonModule,
		MatIconModule,
		MatInputModule,
		MatProgressSpinnerModule,
		MatCheckboxModule,
		ReactiveFormsModule,
		HttpClientModule,
		BrowserAnimationsModule,
		OverlayModule,
	],
	entryComponents: [
		ProgressOverlayComponent,
	],
	providers: [{ provide: OverlayContainer, useClass: FullscreenOverlayContainer }],
	bootstrap: [AppComponent]
})
export class AppModule { }
